var appChat = {};

appChat.EventBus = function() {
	var EventBusConst = function() {
		this._consumers = [];
	};

	EventBusConst.prototype.postMessage = function(message){
	    var callbackArr = this._consumers;
	    callbackArr.forEach(function(callback, callbackArr) {
	      setTimeout(function() {
	          return callback.call(this, message);
	      }, 0);
	    });
	};

	EventBusConst.prototype.registerConsumer = function(callback){
		this._consumers.push(callback);
	};

	var eb1 = new EventBusConst();
	var eb2 = new EventBusConst();

	eb1.registerConsumer(function(message){console.log("Hello " + message + "!")});
	eb1.postMessage("first");

	eb2.registerConsumer(function(message){console.log("Alert " + message + "!")});
	eb2.postMessage("seconds");
};

appChat.EventBus();