var ColorPanel = function(divId, color) {
	var selector = "#" + divId;
	
	return {
		"render": function() {
			console.log("Colorpanel is about to start rendering using selector: " + selector);
			$(selector).html("&nbsp;").css("background-color", color);
		}
	};
}
