var TypeMessages = {
  'MESSAGE_SENT': 'message-sent',
  'MODEL_UPDATED': 'model-updated'
}

var EventBus = function() {
  this.consumers = {};
}

EventBus.prototype.postMessage = function(topic, message){
  var callbackArr = this.consumers[topic];
  callbackArr.forEach(function(callback, callbackArr) {
    setTimeout(function() {
      return callback.call(this, message);
    }, 0);
  });
};

EventBus.prototype.registerConsumer = function(topic, callback){
  if (!this.consumers[topic]) {
    this.consumers[topic] = [];
  }
  this.consumers[topic].push(callback);
};

var eb = new EventBus();