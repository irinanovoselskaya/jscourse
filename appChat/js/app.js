var users = [{'id': 1, 'name': 'Vasya'}, {'id': 2, 'name': 'Masha'}];

var MessageDTO = function(messageObj) {
  this.userId = messageObj.userId;
  this.message = messageObj.message;
}

var Model = function(eb) {
  return [];
};

var ModelServices = function(model, eb) {
  eb.registerConsumer('MESSAGE_SENT', function(messageDTO){
    services.addMessage(messageDTO);
  });
  return {
    'addMessage': function(messageDTO) {
      model.push(messageDTO);
      var modelDTO = this.readAllMessage();
      eb.postMessage('MODEL_UPDATED', modelDTO);
    },
    'readAllMessage': function() {
      var listOfMessageDTO = [];
      model.forEach(function(messageObj, model) {
        var userId = messageObj.userId;
        var messageText = messageObj.message;
        //var messageObjDTO = new MessageDTO({userId: userId, message: messageText});
        listOfMessageDTO.push(new MessageDTO({userId: userId, message: messageText}));
      });
      return listOfMessageDTO;
    }
  }
};

var View = function(users, eb) {
  eb.registerConsumer('MODEL_UPDATED', function(modelDTO){
    view.update(modelDTO);
  });
  return {
    'init': function() {
      $('#chat').html('');
      users.forEach(function(userObj, users) {
        var userId, userName, userHtml;

        userId = userObj.id;
        userName = userObj.name;
        userHtml = '<div class="row"><div class="col-md-12"><h3>' + userName + '</h3></div>'+
          '<div class="col-md-9"><textarea cols="45" rows="8" data-user-id="' + userId + '"></textarea></div>' +
          '<div class="col-md-3"><button class="btn btn-default" data-user-id="' + userId + '">Send</button></div></div>';

        $('#user' + userId).html(userHtml);
        $('.btn[data-user-id="' + userId + '"]').on('click', function(){
          var messageInput = $('textarea[data-user-id="' + userId + '"]');
          var message = messageInput.val();
          if (message !== '') {
            eb.postMessage('MESSAGE_SENT',(new MessageDTO({userId: userId, message: message})));
          }
          messageInput.val('');
        });
      });
    },
    'update': function(modelDTO) {
      $('#chat').html('');
      modelDTO.forEach(function(messageObj, modelDTO) {
        var userId, messageText, messageHtml;

        userId = messageObj.userId;
        messageText = messageObj.message;
        messageHtml = '<div class="col-md-12" data-user-id="' + userId + '"><span>' + messageText + '</span></span></div>';

        $('#chat').append(messageHtml);
      });
    }
  }
};

var model = new Model(); 
var services = new ModelServices(model, eb);
var view = new View(users, eb);  

view.init();